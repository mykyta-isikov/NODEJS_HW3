const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const { User } = require('../models/User');
const { generatePassword } = require('../utils/generatePassword');
const { registerSchema, loginSchema, forgotPasswordSchema } = require('../joi/authSchemas');

const registerUser = async (req, res) => {
  try {
    await registerSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  const { email, role, password } = req.body;
  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });

  try {
    const duplicate = await User.findOne({ email });
    if (duplicate !== null) {
      res.status(400).send({ message: 'This user already exists' });
      return;
    }
    await user.save();
    res.status(200).send({ message: 'Success' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const loginUser = async (req, res) => {
  try {
    await loginSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  try {
    const user = await User.findOne({ email: req.body.email });
    if (user === null) {
      res.status(400).send({ message: 'Not authorized' });
      return;
    }
    if (await bcrypt.compare(req.body.password, user.password) === false) {
      res.status(400).send({ message: 'Not authorized' });
      return;
    }

    const payload = {
      /* eslint-disable-next-line no-underscore-dangle */
      userId: user._id,
      email: user.email,
      role: user.role,
      password: user.password,
    };
    const jwtToken = jwt.sign(payload, process.env.JWT_PASSWORD);
    res.status(200).send({
      message: 'Success',
      jwt_token: jwtToken,
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const forgotPassword = async (req, res) => {
  try {
    await forgotPasswordSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  const newPassword = generatePassword(16);

  try {
    const user = await User.findOneAndUpdate({ email: req.body.email }, {
      password: await bcrypt.hash(newPassword, 10),
    });

    if (user === null) {
      res.status(400).send({ message: 'No user with such email' });
      return;
    }

    const transporter = nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      secure: false,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
      },
    });

    await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>',
      to: req.body.email,
      subject: 'New password',
      text: newPassword,
      html: `<p>Your new password is: <b>${newPassword}</b></p>`,
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
    return;
  }

  res.status(200).send({ message: 'New password sent to your email address' });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};
