const bcrypt = require('bcryptjs');
const { User } = require('../models/User');
const { changePasswordSchema } = require('../joi/usersSchemas');

const getProfile = async (req, res) => {
  try {
    const user = await User.findById(req.user.userId);

    const output = {
      user: {
        /* eslint-disable-next-line no-underscore-dangle */
        _id: user._id,
        role: user.role,
        email: user.email,
        createdDate: user.createdDate,
      },
    };
    res.status(200).send(output);
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const deleteProfile = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user.userId);
    res.status(200).send({ message: 'Success' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const changeProfilePassword = async (req, res) => {
  try {
    await changePasswordSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  try {
    const user = await User.findById(req.user.userId);

    if (await bcrypt.compare(req.body.oldPassword, user.password) === false) {
      res.status(400).send({ message: 'Old password is incorrect' });
      return;
    }

    user.password = await bcrypt.hash(req.body.newPassword, 10);
    await user.save();
    res.status(200).send({ message: 'Password changed successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  getProfile,
  deleteProfile,
  changeProfilePassword,
};
