const { Truck } = require('../models/Truck');
const { addTruckSchema } = require('../joi/trucksSchemas');

const getTrucks = async (req, res) => {
  try {
    const trucks = await Truck.find({ created_by: req.user.userId });
    res.status(200).send({ trucks });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const addTruck = async (req, res) => {
  try {
    await addTruckSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  const truck = new Truck({
    created_by: req.user.userId,
    type: req.body.type,
  });

  try {
    await truck.save();
    res.status(200).send({ message: 'Truck created successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const getTruck = async (req, res) => {
  try {
    const truck = await Truck.findById(req.params.id);
    res.status(200).send({ truck });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const updateTruck = async (req, res) => {
  try {
    await addTruckSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  try {
    await Truck.findByIdAndUpdate(req.params.id, { type: req.body.type });
    res.status(200).send({ message: 'Truck details changed successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const deleteTruck = async (req, res) => {
  try {
    await Truck.findByIdAndDelete(req.params.id);
    res.status(200).send({ message: 'Truck deleted successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const assignTruck = async (req, res) => {
  try {
    if (await Truck.findOne({ assigned_to: req.user.userId })) {
      await Truck.findOneAndUpdate({ assigned_to: req.user.userId }, { assigned_to: '' });
    }
    await Truck.findByIdAndUpdate(req.params.id, { assigned_to: req.user.userId });
    res.status(200).send({ message: 'Truck assigned successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
