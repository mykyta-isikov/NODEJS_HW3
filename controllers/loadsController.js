const { Load } = require('../models/Load');
const { Truck } = require('../models/Truck');

const { addLoadLog } = require('../utils/addLoadLog');
const {
  addLoadSchema,
} = require('../joi/loadsSchemas');

const truckTypes = {
  SPRINTER: {
    length: 300,
    width: 250,
    height: 170,
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    length: 500,
    width: 250,
    height: 170,
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    length: 700,
    width: 350,
    height: 200,
    payload: 4000,
  },
};

const getLoads = async (req, res) => {
  const [offset, limit, status] = [+req.query.offset || 0, +req.query.limit || 0, req.query.status];
  try {
    let filter = {};
    if (req.user.role === 'SHIPPER') filter.created_by = req.user.userId;
    if (req.user.role === 'DRIVER') filter.assigned_to = req.user.userId;
    if (status) filter.status = status;
    const loads = await Load.find(filter).skip(offset).limit(limit);

    res.status(200).send({
      offset,
      limit,
      count: loads.length,
      loads,
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const addLoad = async (req, res) => {
  try {
    await addLoadSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  const load = new Load({
    created_by: req.user.userId,
    name: req.body.name,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: {
      width: req.body.dimensions.width,
      length: req.body.dimensions.length,
      height: req.body.dimensions.height,
    },
  });

  try {
    await addLoadLog(load, 'Load created');
    res.status(200).send({ message: 'Load created successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const getActiveLoad = async (req, res) => {
  try {
    const load = await Load.findOne({ assigned_to: req.user.userId, status: { $ne: 'SHIPPED' } });
    if (load === null) {
      res.status(400).send({ message: 'Active load not found' });
      return;
    }
    res.status(200).send({ load });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const nextLoadState = async (req, res) => {
  const states = Load.schema.path('state').enumValues;

  const load = await Load.findOne({ assigned_to: req.user.userId, status: { $ne: 'SHIPPED' } });
  if (load === null) {
    res.status(400).send({ message: 'Active load not found' });
    return;
  }

  const loadStateIndex = states.indexOf(load.state);
  load.state = states[loadStateIndex + 1];
  await addLoadLog(load, `Load state changed to "${load.state}"`);

  if (load.state === 'Arrived to delivery') {
    load.status = 'SHIPPED';
    await addLoadLog(load, `Load status changed to "${load.status}"`);
    await Truck.findOneAndUpdate({ assigned_to: load.assigned_to }, { status: 'IS' });
  }

  await load.save();

  res.status(200).send({ message: `Load state changed to '${load.state}'` });
};

const getLoad = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    res.status(200).send({ load });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const updateLoad = async (req, res) => {
  try {
    await addLoadSchema.validateAsync(req.body);
  } catch (err) {
    res.status(400).send({ message: err.message });
    return;
  }

  try {
    await Load.findByIdAndUpdate(req.params.id, {
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: {
        width: req.body.dimensions.width,
        length: req.body.dimensions.length,
        height: req.body.dimensions.height,
      },
    });
    res.status(200).send({ message: 'Load details changed successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const deleteLoad = async (req, res) => {
  try {
    await Load.findByIdAndDelete(req.params.id);
    res.status(200).send({ message: 'Load deleted successfully' });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const postLoad = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);

    load.status = 'POSTED';
    await addLoadLog(load, `Load status changed to "${load.status}"`);

    let truckType = '';
    const keys = Object.keys(truckTypes);
    for (let i = 0; i < keys.length; i += 1) {
      if (
        truckTypes[keys[i]].width >= load.dimensions.width
          && truckTypes[keys[i]].length >= load.dimensions.length
          && truckTypes[keys[i]].height >= load.dimensions.height
          && truckTypes[keys[i]].payload >= load.payload
      ) {
        truckType = keys[i];
        break;
      }
    }

    const truck = await Truck.findOne({
      assigned_to: { $ne: '' },
      status: 'IS',
      type: truckType,
    });

    if (truck === null) {
      load.status = 'NEW';
      await addLoadLog(load, `Driver not found. Load status changed to "${load.status}"`);
      res.status(200).send({
        message: 'Driver not found',
        driver_found: false,
      });
      return;
    }

    truck.status = 'OL';
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.assigned_to = truck.assigned_to;
    await addLoadLog(load, `Load assigned to driver with id "${load.assigned_to}"`);
    await addLoadLog(load, `Load state changed to "${load.state}"`);
    await truck.save();

    res.status(200).send({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

const getLoadInfo = async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    const output = {};
    output.load = load;
    if (load.assigned_to !== null) output.truck = await Truck.findById(load.assigned_to);

    res.status(200).send(output);
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadInfo,
};
