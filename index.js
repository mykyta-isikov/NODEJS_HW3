const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const morgan = require('morgan');
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');

const { usersRouter } = require('./routes/usersRouter');
const { trucksRouter } = require('./routes/trucksRouter');
const { loadsRouter } = require('./routes/loadsRouter');
const { authRouter } = require('./routes/authRouter');

const { authMiddleware } = require('./middleware/authMiddleware');
const { driverMiddleware } = require('./middleware/driverMiddleware');

const app = express();

dotenv.config();
mongoose.connect(process.env.DB_CONNECTION_STRING);

app.use(cors());
app.use(express.json());
app.use(morgan('tiny', { stream: fs.createWriteStream('./requests.log', { flags: 'a' }) }));
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/trucks', authMiddleware, driverMiddleware, trucksRouter);
app.use('/api/loads', authMiddleware, loadsRouter);
app.use('/api/auth', authRouter);

const PORT = process.env.PORT || 8080;

app.listen(PORT);
