const { Truck } = require('../models/Truck');

const truckAccess = async (req, res, next) => {
  try {
    const truck = await Truck.findById(req.params.id);

    if (truck === null) {
      res.status(400).send({ message: 'Truck not found' });
      return;
    }
    if (truck.created_by !== req.user.userId) {
      res.status(400).send({ message: 'Access denied' });
      return;
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  truckAccessMiddleware: truckAccess,
};
