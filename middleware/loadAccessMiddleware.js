const { Load } = require('../models/Load');

const loadAccess = async (req, res, next) => {
  try {
    const load = await Load.findById(req.params.id);

    if (load === null) {
      res.status(400).send({ message: 'Load not found' });
      return;
    }
    if (load.created_by !== req.user.userId) {
      res.status(400).send({ message: 'Access denied' });
      return;
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  loadAccessMiddleware: loadAccess,
};
