const { Truck } = require('../models/Truck');

const truckNotAssigned = async (req, res, next) => {
  try {
    const truck = await Truck.findById(req.params.id);

    if (truck.assigned_to) {
      res.status(400).send({ message: 'Cannot modify assigned truck' });
      return;
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  truckNotAssignedMiddleware: truckNotAssigned,
};
