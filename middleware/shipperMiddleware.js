const shipper = async (req, res, next) => {
  if (req.user.role === 'SHIPPER') {
    next();
    return;
  }
  res.status(400).send({ message: 'Access denied' });
};

module.exports = {
  shipperMiddleware: shipper,
};
