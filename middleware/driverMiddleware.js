const driver = async (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    next();
    return;
  }
  res.status(400).send({ message: 'Access denied' });
};

module.exports = {
  driverMiddleware: driver,
};
