const { Load } = require('../models/Load');

const newLoad = async (req, res, next) => {
  try {
    const load = await Load.findById(req.params.id);

    if (load.status !== 'NEW') {
      res.status(400).send({ message: 'Cannot change posted loads' });
      return;
    }

    next();
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

module.exports = {
  newLoadMiddleware: newLoad,
};
