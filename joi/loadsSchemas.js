const Joi = require('joi');

const addLoadSchema = Joi.object({
  name: Joi.string()
    .required(),
  payload: Joi.number()
    .min(0)
    .max(4000)
    .required(),
  pickup_address: Joi.string()
    .required(),
  delivery_address: Joi.string()
    .required(),
  dimensions: Joi.object({
    width: Joi.number()
      .min(0)
      .max(700)
      .required(),
    length: Joi.number()
      .min(0)
      .max(350)
      .required(),
    height: Joi.number()
      .min(0)
      .max(200)
      .required(),
  })
    .required(),
}).required();

module.exports = {
  addLoadSchema,
};
