const Joi = require('joi');

const registerSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string()
    .required(),
  role: Joi.string()
    .allow('SHIPPER', 'DRIVER')
    .required(),
}).required();

const loginSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string()
    .required(),
}).required();

const forgotPasswordSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
}).required();

module.exports = {
  registerSchema,
  loginSchema,
  forgotPasswordSchema,
};
