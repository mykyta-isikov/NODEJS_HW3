const Joi = require('joi');

const addTruckSchema = Joi.object({
  type: Joi.string()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
    .required(),
}).required();

module.exports = {
  addTruckSchema,
};
