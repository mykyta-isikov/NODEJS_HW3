const Joi = require('joi');

const changePasswordSchema = Joi.object({
  oldPassword: Joi.string()
    .required(),
  newPassword: Joi.ref('oldPassword'),
}).required();

module.exports = {
  changePasswordSchema,
};
