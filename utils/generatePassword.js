const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.split('');

const generatePassword = (length) => {
  const password = [];
  for (let i = 0; i < length; i += 1) {
    password.push(characters[Math.floor(Math.random() * characters.length)]);
  }
  return password.join('');
};

module.exports = {
  generatePassword,
};
