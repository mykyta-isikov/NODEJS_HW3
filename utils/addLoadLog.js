const addLoadLog = async (loadObj, logMessage, logTime = new Date()) => {
  const logObject = {
    message: logMessage,
    time: logTime.toISOString(),
  };
  loadObj.logs.push(logObject);
  await loadObj.save();
};

module.exports = {
  addLoadLog,
};
