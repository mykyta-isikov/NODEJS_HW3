# LAB FE NodeJS HW #3

This is an UBER like service for freight trucks, that is made in REST style and uses MongoDB as database. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. Application contains 2 roles, driver and shipper.

## Install

    npm i

## Run the app

    npm start

# REST API

REST API is described in [openapi.yaml](https://gitlab.com/mykyta-isikov/NODEJS_HW3/-/blob/master/openapi.yaml "Open file") file.

# Optional criteria checklist

### Any system user can easily reset his password using 'forgot password' option.

* New password will consist of 16 random alphanumeric characters and will be sent to user's email. 
* But this API sends emails to *Mailtrap* **ONLY**, thus protecting real email addresses from spamming by revealed user credentials in `.env` file. ![mailtrap](public/images/mailtrap.png)

### Ability to filter loads by status

* API user can add `status` query parameter to filter loads by their status value.
* Loads have 4 possible status values: `'NEW'`, `'POSTED'`, `'ASSIGNED'` and `'SHIPPED'`

### Pagination for loads

* API user can add `offset` and `limit` query parameters to implement pagination. The API also returns the length of retrieved loads with `count` property.
* If query parameters are omitted, API will return all available loads. ![pagination](public/images/pagination.png)
