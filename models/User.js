const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: [
      'SHIPPER',
      'DRIVER',
    ],
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date().toISOString(),
    required: true,
  },
});

const User = mongoose.model('User', userSchema);

module.exports = {
  User,
};
