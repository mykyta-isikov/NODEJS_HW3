const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  type: {
    type: String,
    enum: [
      'SPRINTER',
      'SMALL STRAIGHT',
      'LARGE STRAIGHT',
    ],
    required: true,
  },
  status: {
    type: String,
    enum: [
      'IS',
      'OL',
    ],
    default: 'IS',
    required: true,
  },
  createdDate: {
    type: String,
    default: new Date().toISOString(),
    required: true,
  },
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = {
  Truck,
};
