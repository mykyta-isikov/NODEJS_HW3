const express = require('express');

const {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadInfo,
} = require('../controllers/loadsController');
const { driverMiddleware } = require('../middleware/driverMiddleware');
const { loadAccessMiddleware } = require('../middleware/loadAccessMiddleware');
const { shipperMiddleware } = require('../middleware/shipperMiddleware');
const { newLoadMiddleware } = require('../middleware/newLoadMiddleware');

const router = express.Router();

router.get('/', getLoads);
router.post('/', shipperMiddleware, addLoad);
router.get('/active', driverMiddleware, getActiveLoad);
router.patch('/active/state', driverMiddleware, nextLoadState);
router.get('/:id', loadAccessMiddleware, getLoad);
router.put('/:id', loadAccessMiddleware, newLoadMiddleware, updateLoad);
router.delete('/:id', loadAccessMiddleware, newLoadMiddleware, deleteLoad);
router.post('/:id/post', loadAccessMiddleware, newLoadMiddleware, postLoad);
router.get('/:id/shipping_info', loadAccessMiddleware, getLoadInfo);

module.exports = {
  loadsRouter: router,
};
