const express = require('express');
const { truckAccessMiddleware } = require('../middleware/truckAccessMiddleware');
const { truckNotAssignedMiddleware } = require('../middleware/truckNotAssignedMiddleware');

const {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/trucksController');

const router = express.Router();

router.get('/', getTrucks);
router.post('/', addTruck);
router.get('/:id', truckAccessMiddleware, getTruck);
router.put('/:id', truckAccessMiddleware, truckNotAssignedMiddleware, updateTruck);
router.delete('/:id', truckAccessMiddleware, truckNotAssignedMiddleware, deleteTruck);
router.post('/:id/assign', truckAccessMiddleware, assignTruck);

module.exports = {
  trucksRouter: router,
};
