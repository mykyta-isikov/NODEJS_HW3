const express = require('express');

const {
  getProfile,
  deleteProfile,
  changeProfilePassword,
} = require('../controllers/usersController');

const router = express.Router();

router.get('/me', getProfile);
router.delete('/me', deleteProfile);
router.patch('/me/password', changeProfilePassword);

module.exports = {
  usersRouter: router,
};
